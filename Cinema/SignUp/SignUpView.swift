//
//  SignUpView.swift
//  Cinema
//
//  Created by danyaq on 10.09.2021.
//

import SwiftUI

struct SignUpView: View {
    @StateObject var register = Register()
    @Binding var selected: Int
    @State var titleAlert = ""
    @State var email = ""
    @State var password = ""
    @State var firstName = ""
    @State var secondName = ""
    @State var rePassword = ""
    @State var showAlert = false
    @State var msg = ""
    var body: some View {
        ZStack{
            Color.init("bg").edgesIgnoringSafeArea(.all)
            VStack{
                
                Image("l")
                    .resizable()
                    .frame(width: 207, height: 134)
                    .padding(.top, 44)
                Spacer()
                VStack(spacing: 16){
                    ZStack{
                        if firstName.isEmpty{
                            Text("Имя")
                                .frame(maxWidth: .infinity, alignment: .leading)
                        }
                        TextField("", text: $firstName)
                    }
                    .font(.system(size: 14))
                    .foregroundColor(Color.init(#colorLiteral(red: 0.6862745098, green: 0.6862745098, blue: 0.6862745098, alpha: 1)))
                    .padding(.horizontal, 16)
                    .frame(height: 44)
                    .background(Color.init("bg"))
                    .cornerRadius(4)
                    .overlay(RoundedRectangle(cornerRadius: 4).stroke(lineWidth: 1).foregroundColor(Color.init(#colorLiteral(red: 0.6862745098, green: 0.6862745098, blue: 0.6862745098, alpha: 1))))
                    ZStack{
                        if secondName.isEmpty{
                            Text("Фамилия")
                                .frame(maxWidth: .infinity, alignment: .leading)
                        }
                        TextField("", text: $secondName)
                    }
                    .font(.system(size: 14))
                    .foregroundColor(Color.init(#colorLiteral(red: 0.6862745098, green: 0.6862745098, blue: 0.6862745098, alpha: 1)))
                    .padding(.horizontal, 16)
                    .frame(height: 44)
                    .background(Color.init("bg"))
                    .cornerRadius(4)
                    .overlay(RoundedRectangle(cornerRadius: 4).stroke(lineWidth: 1).foregroundColor(Color.init(#colorLiteral(red: 0.6862745098, green: 0.6862745098, blue: 0.6862745098, alpha: 1))))
                    ZStack{
                        if email.isEmpty{
                            Text("E-mail")
                                .frame(maxWidth: .infinity, alignment: .leading)
                        }
                        TextField("", text: $email)
                    }
                    .font(.system(size: 14))
                    .foregroundColor(Color.init(#colorLiteral(red: 0.6862745098, green: 0.6862745098, blue: 0.6862745098, alpha: 1)))
                    .padding(.horizontal, 16)
                    .frame(height: 44)
                    .background(Color.init("bg"))
                    .cornerRadius(4)
                    .overlay(RoundedRectangle(cornerRadius: 4).stroke(lineWidth: 1).foregroundColor(Color.init(#colorLiteral(red: 0.6862745098, green: 0.6862745098, blue: 0.6862745098, alpha: 1))))
                    ZStack{
                        if password.isEmpty{
                            Text("Пароль")
                                .frame(maxWidth: .infinity, alignment: .leading)
                        }
                        TextField("", text: $password)
                    }
                    .font(.system(size: 14))
                    .foregroundColor(Color.init(#colorLiteral(red: 0.6862745098, green: 0.6862745098, blue: 0.6862745098, alpha: 1)))
                    .padding(.horizontal, 16)
                    .frame(height: 44)
                    .background(Color.init("bg"))
                    .cornerRadius(4)
                    .overlay(RoundedRectangle(cornerRadius: 4).stroke(lineWidth: 1).foregroundColor(Color.init(#colorLiteral(red: 0.6862745098, green: 0.6862745098, blue: 0.6862745098, alpha: 1))))
                    ZStack{
                        if rePassword.isEmpty{
                            Text("Повторите пароль")
                                .frame(maxWidth: .infinity, alignment: .leading)
                        }
                        TextField("", text: $rePassword)
                    }
                    .font(.system(size: 14))
                    .foregroundColor(Color.init(#colorLiteral(red: 0.6862745098, green: 0.6862745098, blue: 0.6862745098, alpha: 1)))
                    .padding(.horizontal, 16)
                    .frame(height: 44)
                    .background(Color.init("bg"))
                    .cornerRadius(4)
                    .overlay(RoundedRectangle(cornerRadius: 4).stroke(lineWidth: 1).foregroundColor(Color.init(#colorLiteral(red: 0.6862745098, green: 0.6862745098, blue: 0.6862745098, alpha: 1))))
                }
                .padding(.horizontal, 16)
                Spacer()
                VStack(spacing: 16){
                    Button(action: {
                        if email.isEmpty || password.isEmpty || firstName.isEmpty || secondName.isEmpty || rePassword.isEmpty{
                            showAlert.toggle()
                            titleAlert = "Ошибка"
                            msg = "Заполните все данные"
                        } else {
                            if password != rePassword{
                                showAlert.toggle()
                                titleAlert = "Ошибка"
                                msg = "Пароли не совпадают"
                            } else {
                        register.register(email: email, password: password, firstName: firstName, lastName: secondName){ error in
                            if error != ""{
                                print(error)
                                showAlert.toggle()
                                msg = error
                                titleAlert = "Ошибка"
                            } else {
                                titleAlert = "Успех"
                            }
                        }
                            }
                        }
                    }, label: {
                        Text("Зарегистрироваться")
                            .foregroundColor(Color.white)
                            .font(.system(size: 14, weight: .bold))
                            .frame(height: 44)
                            .frame(maxWidth: .infinity)
                            .background(Color.init("button"))
                            .cornerRadius(4)
                    })
                    .alert(isPresented: $showAlert, content: {
                        Alert(title: Text(titleAlert), message: Text(msg),dismissButton: .default(Text("OK")))
                    })
                    Button(action: {
                        selected = 1
                    }, label: {
                        Text("У меня уже есть аккаунт")
                            .foregroundColor(Color.init("button"))
                            .font(.system(size: 14, weight: .bold))
                            .frame(height: 44)
                            .frame(maxWidth: .infinity)
                            .background(Color.init("bg"))
                            .cornerRadius(4)
                            .overlay(RoundedRectangle(cornerRadius: 4).stroke(lineWidth: 1).foregroundColor(Color.init(#colorLiteral(red: 0.6862745098, green: 0.6862745098, blue: 0.6862745098, alpha: 1))))
                    })
                    
                }
                .padding(.horizontal, 16)
                .padding(.bottom, 44)
            }
        }
    }
}
