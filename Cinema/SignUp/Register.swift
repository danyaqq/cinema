//
//  Register.swift
//  Cinema
//
//  Created by danyaq on 10.09.2021.
//

import Foundation
import SwiftyJSON
import Alamofire



class Register: ObservableObject{
    
    func register(email: String, password: String, firstName: String, lastName: String, com: (( _ Error: String) -> Void)? = nil){
        let url = "http://cinema.areas.su/auth/register"
        let parameters = ["email" : email,
                          "password" : password,
                          "firstName" : firstName,
                          "lastName": lastName]
        AF.request(url, method: .post, parameters: parameters).validate().responseJSON{ resonse in
            switch resonse.result{
            case .success(let value):
                let json = JSON(value)
                print(json)
                com!("")
            case .failure(let error):
                print(error.localizedDescription)
                com!(error.localizedDescription)
            }
        }
    }
    
}
