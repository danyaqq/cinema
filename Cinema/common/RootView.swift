//
//  RootView.swift
//  Cinema
//
//  Created by danyaq on 10.09.2021.
//

import SwiftUI

struct RootView: View {
    @State var selected = 1
    var body: some View {
        ZStack{
            Color.init("bg").edgesIgnoringSafeArea(.all)
        if selected == 1{
            SignInView(selected: $selected)
        } else if selected == 2{
            SignUpView(selected: $selected)
        } else if selected == 3 {
            MainTabView(selected: $selected)
        }
        }
    }
}

struct RootView_Previews: PreviewProvider {
    static var previews: some View {
        RootView()
    }
}
