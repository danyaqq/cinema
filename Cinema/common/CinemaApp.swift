//
//  CinemaApp.swift
//  Cinema
//
//  Created by danyaq on 10.09.2021.
//

import SwiftUI

@main
struct CinemaApp: App {
    var body: some Scene {
        WindowGroup {
            RootView()
        }
    }
}
