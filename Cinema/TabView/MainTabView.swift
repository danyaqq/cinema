//
//  MainTabView.swift
//  Cinema
//
//  Created by danyaq on 10.09.2021.
//

import SwiftUI

struct MainTabView: View {
    @Binding var selected: Int
    @State var selection = 1
    init(selected: Binding<Int>){
        self._selected = selected
        UITabBar.appearance().isHidden = true
    }
    var body: some View {
        NavigationView{
        VStack(spacing: 0){
            
        TabView(selection: $selection,
                content:  {
                    MainScreenView()
                        .tag(1)
                    Text("Подборка")
                        .tag(2)
                    Text("Коллекции")
                        .tag(3)
                    Text("Профиль")
                        .tag(4)
                })
            .tabViewStyle(DefaultTabViewStyle())
            CustomTabView(selection: $selection)
            }
        .navigationBarHidden(true)
        .navigationBarBackButtonHidden(true)
       
        }
    }
}


struct CustomTabView: View{
    @Binding var selection: Int
    var body: some View{
        HStack{
            Button(action: {
                selection = 1
            }, label: {
                VStack(spacing: 5){
                    if selection == 1{
                        Image("1")
                    } else {
                    Image("1")
                        .renderingMode(.template)
                        .foregroundColor(Color.init(#colorLiteral(red: 0.4588235294, green: 0.4588235294, blue: 0.4588235294, alpha: 1)))
                    }
                    Text("Главное")
                        .font(.system(size: 10, weight: .medium))
                        .foregroundColor(selection == 1 ? Color.init("button") : Color.init(#colorLiteral(red: 0.4588235294, green: 0.4588235294, blue: 0.4588235294, alpha: 1)))
                }
                .frame(maxWidth: .infinity, alignment: .center)
            })
            Button(action: {
                selection = 2
            }, label: {
                VStack(spacing: 5){
                    Image("2")
                        .renderingMode(.template)
                        .foregroundColor(selection == 2 ? Color.init("button") : Color.init(#colorLiteral(red: 0.4588235294, green: 0.4588235294, blue: 0.4588235294, alpha: 1)))
                    Text("Подборка")
                        .font(.system(size: 10, weight: .medium))
                        .foregroundColor(selection == 2 ? Color.init("button") : Color.init(#colorLiteral(red: 0.4588235294, green: 0.4588235294, blue: 0.4588235294, alpha: 1)))
                }
                .frame(maxWidth: .infinity, alignment: .center)
            })
            Button(action: {
                selection = 3
            }, label: {
                VStack(spacing: 5){
                    Image("3")
                        .renderingMode(.template)
                        .foregroundColor(selection == 3 ? Color.init("button") : Color.init(#colorLiteral(red: 0.4588235294, green: 0.4588235294, blue: 0.4588235294, alpha: 1)))
                    Text("Коллекции")
                        .font(.system(size: 10, weight: .medium))
                        .foregroundColor(selection == 3 ? Color.init("button") : Color.init(#colorLiteral(red: 0.4588235294, green: 0.4588235294, blue: 0.4588235294, alpha: 1)))
                }
                .frame(maxWidth: .infinity, alignment: .center)
            })
            Button(action: {
                selection = 4
            }, label: {
                VStack(spacing: 5){
                    Image("4")
                        .renderingMode(.template)
                        .foregroundColor(selection == 4 ? Color.init("button") : Color.init(#colorLiteral(red: 0.4588235294, green: 0.4588235294, blue: 0.4588235294, alpha: 1)))
                    Text("Профиль")
                        .font(.system(size: 10, weight: .medium))
                        .foregroundColor(selection == 4 ? Color.init("button") : Color.init(#colorLiteral(red: 0.4588235294, green: 0.4588235294, blue: 0.4588235294, alpha: 1)))
                }
                .frame(maxWidth: .infinity, alignment: .center)
            })
        }
        .padding(.top, 8)
        .frame(maxWidth: .infinity)
        .background(Color.init(#colorLiteral(red: 0.1098039216, green: 0.1019607843, blue: 0.09803921569, alpha: 1)).edgesIgnoringSafeArea(.bottom))
    }
}


