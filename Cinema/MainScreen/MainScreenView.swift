//
//  MainScreenView.swift
//  Cinema
//
//  Created by danyaq on 10.09.2021.
//

import SwiftUI
import SDWebImageSwiftUI

struct MainScreenView: View {
    @StateObject var cover = CoverViewModel()
    @StateObject var movie = MovieViewModel()
    @State var itemModel: MovieModel?
    @State var showDetail = false
    var body: some View {
       
   
            ZStack{
                Color.init("bg").edgesIgnoringSafeArea(.all)
        ScrollView(.vertical, showsIndicators: false, content: {
            VStack(spacing: 0){
                WebImage(url: URL(string: "http://cinema.areas.su/up/images/\(cover.cover?.backgroundImage ?? "")"))
                    .resizable()
                    .frame(height: 400)
                    .frame(width: UIScreen.main.bounds.width)
                    .overlay(Button(action: {
                        
                    }, label: {
                        Text("Смотреть")
                            .frame(width: 134, height: 44)
                            .font(.system(size: 14, weight: .bold))
                            .foregroundColor(Color.white)
                           
                            .background(Color.init("button"))
                            .cornerRadius(4)
                    }).padding(.bottom, 63), alignment: .bottom)
                VStack(alignment: .leading, spacing: 16){
                    Text("В тренде")
                        .font(.system(size: 24, weight: .bold))
                        .foregroundColor(Color.init("button"))
                        .padding(.horizontal, 16)
                    ScrollView(.horizontal, showsIndicators: false, content: {
                        LazyHStack(spacing: 16){
                            ForEach(movie.inTrend, id: \.self){image in
                                NavigationLink(
                                    destination: MovieScreenView(model: image),
                                    label: {
                                        WebImage(url: URL(string: "http://cinema.areas.su/up/images/\(image.poster)"))
                                            .resizable()
                                            .frame(width: 100, height: 144)
                                            
                                    })
                                
                            }
                        }
                        .padding(.horizontal, 16)
                    })
                }
                .padding(.vertical, 32)
              //Вы смотрели
                VStack(alignment: .leading, spacing: 16){
                    Text("Вы смотрели")
                        .font(.system(size: 24, weight: .bold))
                        .foregroundColor(Color.init("button"))
                        .padding(.horizontal, 16)
                    WebImage(url: URL(string: "http://cinema.areas.su/up/images/\(cover.cover?.backgroundImage ?? "")"))
                        .resizable()
                        .frame(height: 240)
                        .frame(width: UIScreen.main.bounds.width)
                }
                
                VStack(alignment: .leading, spacing: 16){
                    Text("Новое")
                        .font(.system(size: 24, weight: .bold))
                        .foregroundColor(Color.init("button"))
                        .padding(.horizontal, 16)
                    ScrollView(.horizontal, showsIndicators: false, content: {
                        LazyHStack(spacing: 16){
                            ForEach(movie.new, id: \.self){image in
                                NavigationLink(
                                    destination: MovieScreenView(model: image),
                                    label: {
                                        WebImage(url: URL(string: "http://cinema.areas.su/up/images/\(image.poster)"))
                                            .resizable()
                                            .frame(width: 240, height: 144)
                                            
                                    })
                            }
                        }
                        .padding(.horizontal, 16)
                    })
                }
                .padding(.top, 32)
                VStack(alignment: .leading, spacing: 16){
                    Text("Для вас")
                        .font(.system(size: 24, weight: .bold))
                        .foregroundColor(Color.init("button"))
                        .padding(.horizontal, 16)
                    ScrollView(.horizontal, showsIndicators: false, content: {
                        LazyHStack(spacing: 16){
                            ForEach(movie.forMe, id: \.self){image in
                                NavigationLink(
                                    destination: MovieScreenView(model: image),
                                    label: {
                                        WebImage(url: URL(string: "http://cinema.areas.su/up/images/\(image.poster)"))
                                            .resizable()
                                            .frame(width: 100, height: 144)
                                            
                                    })
                            }
                        }
                        .padding(.horizontal, 16)
                    })
                }
                .padding(.top, 32)
                Button(action: {
                    
                }, label: {
                    Text("Указать интересы")
                        .font(.system(size: 14, weight: .bold))
                        .foregroundColor(Color.white)
                        .frame(height: 44)
                        .frame(maxWidth: .infinity)
                        .background(Color.init("button"))
                        .cornerRadius(4)
                })
                .padding(.vertical, 44)
                .padding(.horizontal, 16)
            }
           
        })
        .navigationBarHidden(true)
        .navigationBarBackButtonHidden(true)
        .edgesIgnoringSafeArea(.top)
        }
        
    }
}

struct MainScreenView_Previews: PreviewProvider {
    static var previews: some View {
        MainScreenView()
    }
}
