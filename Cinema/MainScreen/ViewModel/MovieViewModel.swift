//
//  MovieViewModel.swift
//  Cinema
//
//  Created by danyaq on 10.09.2021.
//

import Foundation



import Alamofire



class MovieViewModel: ObservableObject{
    @Published var inTrend: [MovieModel] = []
    @Published var new: [MovieModel] = []
    @Published var forMe: [MovieModel] = []
    init(){
        getCover(category: "inTrend")
        getCover(category: "new")
        getCover(category: "forMe")
    }
    func getCover(category: String){
        let url = "http://cinema.areas.su/movies?filter=\(category)"
   
        AF.request(url, method: .get).validate().responseJSON{ resonse in
            switch resonse.result{
            case .success:
                
                let decoder = try? JSONDecoder().decode([MovieModel].self, from: resonse.data!)
                DispatchQueue.main.async {
                    if category == "inTrend"{
                        self.inTrend = decoder!
                    }
                    
                    if category == "new"{
                        self.new = decoder!
                    }
                    
                    if category == "forMe"{
                        self.forMe = decoder!
                    }
                }
              
                
            case .failure(let error):
                print(error.localizedDescription)
              
            }
        }
    }
    
}
