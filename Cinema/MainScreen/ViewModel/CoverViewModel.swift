//
//  CoverViewModel.swift
//  Cinema
//
//  Created by danyaq on 10.09.2021.
//

import Foundation
import SwiftyJSON
import Alamofire



class CoverViewModel: ObservableObject{
    @Published var cover : CoverModel?
    init(){
        getCover()
    }
    func getCover(){
        let url = "http://cinema.areas.su/movies/cover"
   
        AF.request(url, method: .get).validate().responseJSON{ resonse in
            switch resonse.result{
            case .success:
                
                let decoder = try? JSONDecoder().decode(CoverModel.self, from: resonse.data!)
                self.cover = decoder
                print(self.cover ?? "no data")
            case .failure(let error):
                print(error.localizedDescription)
              
            }
        }
    }
    
}
