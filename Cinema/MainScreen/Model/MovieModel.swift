//
//  MovieModel.swift
//  Cinema
//
//  Created by danyaq on 10.09.2021.
//

import Foundation


struct MovieModel: Hashable, Codable {
    var movieId, name, description, age, poster: String
    var images: [String]
    var tags: [TagModel]
}

struct TagModel: Hashable, Codable{
    var idTags, tagName: String
}
