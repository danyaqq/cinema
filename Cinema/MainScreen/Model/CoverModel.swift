//
//  CoverModel.swift
//  Cinema
//
//  Created by danyaq on 10.09.2021.
//

import Foundation


struct CoverModel: Hashable, Codable {
    var movieId, backgroundImage, foregroundImage: String
}
