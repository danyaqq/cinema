//
//  Auth.swift
//  Cinema
//
//  Created by danyaq on 10.09.2021.
//

import Foundation
import SwiftyJSON
import Alamofire



class Auth: ObservableObject{
    
    func auth(email: String, password: String, com: (( _ token: String, _ Error: String) -> Void)? = nil){
        let url = "http://cinema.areas.su/auth/login"
        let parameters = ["email" : email,
                          "password" : password]
        AF.request(url, method: .post, parameters: parameters).validate().responseJSON{ resonse in
            switch resonse.result{
            case .success(let value):
                let json = JSON(value)
                let token = json["token"].stringValue
                UserDefaults.standard.setValue(token, forKey: "token")
                com!(token, "")
            case .failure(let error):
                print(error.localizedDescription)
                com!("", error.localizedDescription)
            }
        }
    }
    
}
