//
//  SignInView.swift
//  Cinema
//
//  Created by danyaq on 10.09.2021.
//

import SwiftUI

struct SignInView: View {
    @StateObject var auth = Auth()
    @Binding var selected: Int
    @State var email = "vasya@mail.com"
    @State var password = "qwerty"
    @State var showAlert = false
    @State var msg = ""
    var body: some View {
        ZStack{
            Color.init("bg").edgesIgnoringSafeArea(.all)
            VStack(spacing: 0){
                Spacer()
                VStack(spacing: 104){
                Image("l")
                    .resizable()
                    .frame(width: 207, height: 134)
                
                }
                Spacer()
                VStack(spacing: 16){
                    ZStack{
                        if email.isEmpty{
                            Text("E-mail")
                                .frame(maxWidth: .infinity, alignment: .leading)
                        }
                        TextField("", text: $email)
                    }
                    .font(.system(size: 14))
                    .foregroundColor(Color.init(#colorLiteral(red: 0.6862745098, green: 0.6862745098, blue: 0.6862745098, alpha: 1)))
                    .padding(.horizontal, 16)
                    .frame(height: 44)
                    .background(Color.init("bg"))
                    .cornerRadius(4)
                    .overlay(RoundedRectangle(cornerRadius: 4).stroke(lineWidth: 1).foregroundColor(Color.init(#colorLiteral(red: 0.6862745098, green: 0.6862745098, blue: 0.6862745098, alpha: 1))))
                    ZStack{
                        if password.isEmpty{
                            Text("Пароль")
                                .frame(maxWidth: .infinity, alignment: .leading)
                        }
                        TextField("", text: $password)
                    }
                    .font(.system(size: 14))
                    .foregroundColor(Color.init(#colorLiteral(red: 0.6862745098, green: 0.6862745098, blue: 0.6862745098, alpha: 1)))
                    .padding(.horizontal, 16)
                    .frame(height: 44)
                    .background(Color.init("bg"))
                    .cornerRadius(4)
                    .overlay(RoundedRectangle(cornerRadius: 4).stroke(lineWidth: 1).foregroundColor(Color.init(#colorLiteral(red: 0.6862745098, green: 0.6862745098, blue: 0.6862745098, alpha: 1))))
                }
                .padding(.horizontal, 16)
                Spacer()
                VStack(spacing: 16){
                    Button(action: {
                        if email.isEmpty || password.isEmpty{
                            showAlert.toggle()
                            msg = "Введите логин и пароль"
                        } else {
                        auth.auth(email: email, password: password){token, error in
                            if error != ""{
                                print(error)
                                msg = error
                                showAlert.toggle()
                            } else {
                                print(token)
                                selected = 3
                            }
                        }
                        }
                    }, label: {
                        Text("Войти")
                            .foregroundColor(Color.white)
                            .font(.system(size: 14, weight: .bold))
                            .frame(height: 44)
                            .frame(maxWidth: .infinity)
                            .background(Color.init("button"))
                            .cornerRadius(4)
                    })
                    .alert(isPresented: $showAlert, content: {
                        Alert(title: Text("Ошибка"), message: Text(msg),dismissButton: .default(Text("OK")))
                    })
                    Button(action: {
                        selected = 2
                    }, label: {
                        Text("Регистрация")
                            .foregroundColor(Color.init("button"))
                            .font(.system(size: 14, weight: .bold))
                            .frame(height: 44)
                            .frame(maxWidth: .infinity)
                            .background(Color.init("bg"))
                            .cornerRadius(4)
                            .overlay(RoundedRectangle(cornerRadius: 4).stroke(lineWidth: 1).foregroundColor(Color.init(#colorLiteral(red: 0.6862745098, green: 0.6862745098, blue: 0.6862745098, alpha: 1))))
                    })
                    
                }
                .padding(.horizontal, 16)
                .padding(.bottom, 44)
            }
        }
    }
}

