//
//  MovieScreenView.swift
//  Cinema
//
//  Created by danyaq on 10.09.2021.
//

import SwiftUI
import SDWebImageSwiftUI
import AVKit

struct MovieScreenView: View {
    @Environment(\.presentationMode) var presentationMode
    @StateObject var currentFilm = CurrentFilm()
    @StateObject var episode = MovieEpisode()
    @State var play = false
    var model: MovieModel
    var body: some View {
        ZStack{
            Color.init("bg").edgesIgnoringSafeArea(.all)
        ScrollView(.vertical, showsIndicators: false, content: {
            VStack(spacing: 0){
                VStack{
                    HStack{
                        Button(action: {
                            presentationMode.wrappedValue.dismiss()
                        }, label: {
                            Image("Arrow")
                        })
                        Spacer()
                        HStack(spacing: 16){
                            
                            Image("\(currentFilm.film.age)+")
                            Button(action: {
                                
                            }, label: {
                                Image("uil_comments")
                            })
                            
                        }
                    }
                    .overlay(Text(currentFilm.film.name).font(.system(size: 17, weight: .semibold)).foregroundColor(Color.white), alignment: .center)
                    .padding(.leading, 8)
                    .padding(.trailing, 16)
                    .padding(.top, 44)
                    .padding(.top)
                    Spacer()
                    HStack(spacing: 9){
                        ForEach(currentFilm.film.tags, id: \.self){ tag in
                            Text(tag.tagName)
                                .font(.system(size: 14, weight: .regular))
                                .foregroundColor(Color.white)
                                .padding(.horizontal, 16)
                                .frame(height: 24)
                                .background(Color.init("button"))
                                .cornerRadius(4)
                               
                        }
                    }
                    .padding(.bottom, 24)
                    .padding(.horizontal, 16)
                }
                .frame(height: 400)
                .background(WebImage(url: URL(string: "http://cinema.areas.su/up/images/\(currentFilm.film.poster)")).resizable().frame(height: 400))
                if !episode.episode.first!.preview.isEmpty{
                    VStack(alignment: .leading, spacing: 16){
                        Text("Смотреть")
                            .foregroundColor(Color.white)
                            .font(.system(size: 24, weight: .bold))
                            .padding(.leading, 16)
                        CustomVideoPlayer(url: episode.episode.first!.preview)
                            .frame(width: UIScreen.main.bounds.width)
                            .frame(height: 210)
                        
                    }
                    .padding(.top, 16)
                }
             
               
                VStack(alignment: .leading, spacing: 8){
                    Text("Описание")
                        .foregroundColor(Color.white)
                        .font(.system(size: 24, weight: .bold))
                    Text(currentFilm.film.description)
                        .foregroundColor(Color.white)
                        .font(.system(size: 14))
                }
                .padding(.horizontal, 16)
                .padding(.vertical, 32)
                VStack(alignment: .leading, spacing: 16){
                    Text("Кадры")
                        .foregroundColor(Color.white)
                        .font(.system(size: 24, weight: .bold))
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .padding(.leading, 16)
                    ScrollView(.horizontal, showsIndicators: false, content: {
                        HStack(spacing:16){
                            ForEach(currentFilm.film.images, id: \.self){ item in
                                WebImage(url: URL(string: "http://cinema.areas.su/up/images/\(item)"))
                                    .resizable().frame(width: 128, height: 72)
                            }
                        }
                        .padding(.horizontal, 16)
                    })
                  
                }
                .padding(.bottom, 32)
                VStack(alignment: .leading, spacing: 16){
                    Text("Эпизоды")
                        .foregroundColor(Color.white)
                        .font(.system(size: 24, weight: .bold))
                        .frame(maxWidth: .infinity, alignment: .leading)
                    
                        VStack(spacing:16){
                            ForEach(episode.episode, id: \.self){ item in
                                HStack(spacing:16){
                                    if !item.preview.isEmpty{
                                        ZStack{
                                            CustomVideoPlayer(url: item.preview)
                                            if play{
                                                Button(action: {
                                                    play.toggle()
                                                    
                                                }, label: {
                                                    Text("play")
                                                })
                                            } else {
                                                Button(action: {
                                                    play.toggle()
                                                    
                                                }, label: {
                                                    Text("pause")
                                                })
                                            }
                                        }
                                      
                                    } else {
                                        EmptyView()
                                            .frame(width: 128, height: 72)
                                    }
                                    
                           
                                        
                                    VStack(alignment: .leading, spacing: 12){
                                        Text(item.name)
                                            .foregroundColor(Color.white)
                                            .font(.system(size: 14, weight: .bold))
                                        VStack(alignment: .leading, spacing: 6){
                                            Text(item.description)
                                            .foregroundColor(Color.init(#colorLiteral(red: 0.6862745098, green: 0.6862745098, blue: 0.6862745098, alpha: 1)))
                                            .font(.system(size: 12, weight: .regular))
                                            .multilineTextAlignment(.leading)
                                            .lineLimit(2)
                                            Text(item.year)
                                                .foregroundColor(Color.init(#colorLiteral(red: 0.6862745098, green: 0.6862745098, blue: 0.6862745098, alpha: 1)))
                                                .font(.system(size: 12, weight: .regular))
                                        }
                                    }
                                }
                                .onTapGesture {
                                    print(episode.episode)
                                }
                            }
                        }
                    
                    
                  
                }
                .padding(.horizontal, 16)
                .padding(.bottom)
            }
        })
        .navigationBarHidden(true)
        .navigationBarBackButtonHidden(true)
        }
        .edgesIgnoringSafeArea(.top)
        .onAppear(perform: {
            currentFilm.getCurrentMovie(movieId: model.movieId)
            episode.getEpisode(movieId: model.movieId)
          
        })
    }
}


struct CustomVideoPlayer: UIViewControllerRepresentable{
    var url: String

    func makeUIViewController(context: Context) -> AVPlayerViewController {
        let video = AVPlayerViewController()
        video.player = AVPlayer(url: URL(string: "http://cinema.areas.su/up/movies/\(url.replacingOccurrences(of: " ", with: ""))")!)
        video.showsPlaybackControls = false
        video.showsTimecodes = true
        return video
    }
    
    func updateUIViewController(_ uiViewController: AVPlayerViewController, context: Context) {
        
    }
    
    typealias UIViewControllerType = AVPlayerViewController
    
    

}
