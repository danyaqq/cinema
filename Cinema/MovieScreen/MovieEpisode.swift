//
//  MovieEpisode.swift
//  Cinema
//
//  Created by danyaq on 10.09.2021.
//

import Foundation
import Alamofire

class MovieEpisode: ObservableObject{
    @Published var episode: [EpisodeModel] = []
    func getEpisode(movieId: String){
        let url = "http://cinema.areas.su/movies/\(movieId)/episodes"
        AF.request(url, method: .get).validate().responseJSON{ resonse in
            
            switch resonse.result{
            case .success:
                do{
                let decoder = try JSONDecoder().decode([EpisodeModel].self, from: resonse.data!)
                
                DispatchQueue.main.async {
                        self.episode = decoder
                }
                } catch let error{
                    print(error.localizedDescription)
                }
                
            case .failure(let error):
                print(error.localizedDescription)
              
            }
        }
    }
}
