//
//  EpisodeModel.swift
//  Cinema
//
//  Created by danyaq on 10.09.2021.
//

import Foundation


struct EpisodeModel: Hashable, Codable{
    var episodeId,name,description,director,year,runtime,preview,moviesId: String
    var images:[String]
    var stars:[String]
}
