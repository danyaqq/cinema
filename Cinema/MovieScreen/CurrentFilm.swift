//
//  CurrentFilm.swift
//  Cinema
//
//  Created by danyaq on 10.09.2021.
//

import Foundation
import Alamofire

class CurrentFilm: ObservableObject{
    @Published var film: MovieModel = MovieModel(movieId: "", name: "", description: "", age: "", poster: "", images: [""], tags: [TagModel(idTags: "", tagName: "")])
    
    func getCurrentMovie(movieId: String){
        
        let url = "http://cinema.areas.su/movies/\(movieId)"
   
        AF.request(url, method: .get).validate().responseJSON{ resonse in
            switch resonse.result{
            case .success:
                do{
                let decoder = try JSONDecoder().decode(MovieModel.self, from: resonse.data!)
                
                DispatchQueue.main.async {
                        self.film = decoder
                }
                } catch let error{
                    print(error.localizedDescription)
                }
                
            case .failure(let error):
                print(error.localizedDescription)
              
            }
        }
    }
}
